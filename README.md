# README #

This README Contains a Sketch file that showcases the UX process used to 
create the redesign effort of the MichaelFugate.com website (created with Sketch version 52.3)

(PLEASE NOTE: The file size of this Sketch file is 60 megabytes.)

Also included is a PDF version of the Sketch file content (file size 36 megabytes).